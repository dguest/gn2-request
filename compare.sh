#!/usr/bin/env bash

set -eu

H=$PWD

D=$(mktemp -d)
cd $D
MC20=20
MC23=23
CAL23=cal
RANGES=ranges

awk -F . '$1 ~ "mc20_" {print $2}' $H/algs.md | sort -u > $MC20
awk -F . '$1 ~ "mc23_" {print $2}' $H/algs.md | sort -u > $MC23
while read RNG; do
    seq ${RNG%*-*} ${RNG#*-*} >> ${CAL23}-tmp
done < <(egrep '[0-9]+-[0-9]+' $H/calib.txt)
awk '$0' $H/calib.txt | sort -u >> $CAL23-tmp
sort -u ${CAL23}-tmp > $CAL23

wc $MC20 $MC23 $CAL23

echo "20 not 23"
comm -23 $MC20 $MC23 | wc -l
echo "23 not 20"
comm -13 $MC20 $MC23 | wc -l
echo "both"
comm -12 $MC20 $MC23 | wc -l

echo "in 23 and cal"
comm -12 $MC23 $CAL23 | wc -l
echo "in 23 and not cal"
comm -23 $MC23 $CAL23 | wc -l
for uni in $(comm -23 $MC23 $CAL23); do
    grep $uni $H/algs.md
done

